package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"regexp"
	"strconv"
)

func GetUser(apiKey, userId, mode, userType, eventDays string) (User, error) {
	var user User
	var userBuffer []User
	var bodyBuffer []byte
	var params string
	var err error

	if apiKey == "" {
		err = errors.New("no api key provided")
		return user, err
	} else {
		params += "get_user?k=" + apiKey
	}

	if userId == "" {
		err = errors.New("no User id provided")
		return user, err
	} else {
		params += "&u=" + userId
	}

	if umode, err := strconv.Atoi(mode); err != nil || umode < 0 || umode > 3 {
		mode = "1"
	}
	params += "&m=" + mode

	if userType == "string" || userType == "id" {
		params += "&type=" + userType
	}

	if eventDays != "" {
		if days, err := strconv.Atoi(eventDays); err == nil {
			if days >= 1 && days <= 31 {
				params += "&event_days=" + eventDays
			}
		}
	}

	err = nil
	resp, err := restGet(params)
	if err != nil {
		return user, err
	}

	bodyBuffer, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return user, err
	}

	err = json.Unmarshal(bodyBuffer, &userBuffer)
	if err != nil {
		return user, err
	}

	if len(bodyBuffer) == 0 || len(userBuffer) == 0 {
		err = errors.New("empty response form api")
		return user, err
	}

	user = userBuffer[0]
	return user, nil

}

func GetBeatmaps(apiKey, since, setID, mapID, userID, userType, mode, includeConverts, mapHash, limit, mods string) ([]Beatmap, error) {
	var beatmaps []Beatmap
	var bodyBuffer []byte
	var params string

	if apiKey == "" {
		err := errors.New("no api key provided")
		return beatmaps, err
	} else {
		params += "get_beatmaps?k=" + apiKey
	}

	if since != "" {
		match, err := regexp.Match(`(\d{4}[-]\d{2}[-]\d{2})`, []byte(since))
		if err == nil && match == true {
			params += "&since=" + since
		}
	}

	if setID != "" {
		params += "&s=" + setID
	}

	if mapID != "" {
		params += "&b=" + mapID
	}

	if userID != "" {
		params += "&u=" + userID
		if userType == "string" || userType == "id" {
			params += "&type=" + userType
		}
	}

	if umode, err := strconv.Atoi(mode); err != nil || umode < 0 || umode > 3 {
		mode = "1"
	}
	params += "&m=" + mode

	if ucvrt, err := strconv.Atoi(includeConverts); err != nil || ucvrt < 0 || ucvrt > 1 {
		includeConverts = "0"
	}
	params += "&a=" + includeConverts

	if mapHash != "" && validateMD5(mapHash) {
		params += "&h=" + mapHash
	}

	if ulimit, err := strconv.Atoi(limit); err != nil || ulimit < 0 || ulimit > 500 {
		limit = "500"
	}
	params += "&limit=" + limit

	if umods, err := strconv.Atoi(mods); err != nil || umods < 0 || umods > 4294967295 {
		mods = "0"
	}
	params += "&mods=" + mods

	resp, err := restGet(params)
	if err != nil {
		return beatmaps, err
	}

	bodyBuffer, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return beatmaps, err
	}

	err = json.Unmarshal(bodyBuffer, &beatmaps)
	if err != nil {
		return beatmaps, err
	}

	if len(bodyBuffer) == 0 || len(beatmaps) == 0 {
		err = errors.New("empty response form api")
		return beatmaps, err
	}

	return beatmaps, nil
}

func validateMD5(hash string) bool {
	match, _ := regexp.Match(`([a-fA-F0-9]{32})`, []byte(hash))
	return match
}
