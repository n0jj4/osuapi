package main

import "fmt"

func main() {
	//Main, WIP, For testing
	//GetUser usage
	user, err := GetUser("", "Ayato k", "1", "string", "")
	if err != nil {
		fmt.Printf("%v", err)
		return
	}
	fmt.Printf("%+v", user)

	//GetBeatmaps usage
	beatmaps, err := GetBeatmaps("", "", "", "", "9546912", "id", "3", "", "", "", "")
	if err != nil {
		fmt.Printf("%v", err)
		return
	}
	fmt.Printf("%+v", beatmaps)
	return
}
