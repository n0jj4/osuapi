package main

import (
	"net/http"
	"time"
)

func restGet(params string) (*http.Response, error) {
	url := "https://osu.ppy.sh/api/" + params
	var httpClient = http.Client{
		Timeout: 10 * time.Second,
	}
	resp, err := httpClient.Get(url)
	return resp, err
}
