package main

var modes = []string{
	"Nomod",
	"NF",
	"EZ",
	"TouchDevice",
	"HD",
	"HR",
	"SD",
	"DT",
	"RX",
	"HT",
	"NC", // Only set along with DoubleTime. i.e: NC only gives 576
	"FL",
	"AP",
	"SO",
	"RX2", // Autopilot
	"PF",  // Only set along with SuddenDeath. i.e: PF only gives 16416
	"4K",
	"5K",
	"6K",
	"7K",
	"8K",
	"FadeIn",
	"Random",
	"Cinema",
	"Target",
	"9K",
	"Coop",
	"1K",
	"3K",
	"2K",
	"ScoreV2",
	"LastMode",
}

type User struct {
	User_id              string
	Username             string
	Join_date            string // In UTC
	Count300             string // Total amount for all ranked, approved, and loved beatmaps played
	Count100             string // Total amount for all ranked, approved, and loved beatmaps played
	Count50              string // Total amount for all ranked, approved, and loved beatmaps played
	Playcount            string // Only counts ranked, approved, and loved beatmaps
	Ranked_score         string // Counts the best individual score on each ranked, approved, and loved beatmaps
	Total_score          string // Counts every score on ranked, approved, and loved beatmaps
	Pp_rank              string
	Level                string
	Pp_raw               string // For inactive players this will be 0 to purge them from leaderboards
	Accuracy             string
	Count_rank_ss        string
	Count_rank_ssh       string
	Count_rank_s         string // Counts for SS/SSH/S/SH/A ranks on maps
	Count_rank_sh        string
	Count_rank_a         string
	Country              string // Uses the ISO3166-1 alpha-2 country code naming. See this for more information: http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2/)
	Total_seconds_played string
	Pp_country_rank      string // The user's rank in the country.
	Events               []struct {
		Display_html  string
		Beatmap_id    string
		Beatmapset_id string
		Date          string // In UTC
		Epicfactor    string // How "epic" this event is (between 1 and 32)
	}
}

type Beatmap struct {
	Approved         string // 4 = loved, 3 = qualified, 2 = approved, 1 = ranked, 0 = pending, -1 = WIP, -2 = graveyard
	Submit_date      string // date submitted, in UTC
	Approved_date    string // date ranked, in UTC
	Last_update      string // last update date, in UTC. May be after approved_date if map was unranked and reranked.
	Artist           string
	Beatmap_id       string // beatmap_id is per difficulty
	Beatmapset_id    string // beatmapset_id groups difficulties into a set
	Bpm              string
	Creator          string
	Creator_id       string
	Difficultyrating string // The amount of stars the map would have ingame and on the website
	Diff_aim         string
	Diff_speed       string
	Diff_size        string // Circle size value (CS)
	Diff_overall     string // Overall difficulty (OD)
	Diff_approach    string // Approach Rate (AR)
	Diff_drain       string // Health drain (HP)
	Hit_length       string // seconds from first note to last note not including breaks
	Source           string
	Genre_id         string // 0 = any, 1 = unspecified, 2 = video game, 3 = anime, 4 = rock, 5 = pop, 6 = other, 7 = novelty, 9 = hip hop, 10 = electronic (note that there's no 8)
	Language_id      string // 0 = any, 1 = other, 2 = english, 3 = japanese, 4 = chinese, 5 = instrumental, 6 = korean, 7 = french, 8 = german, 9 = swedish, 10 = spanish, 11 = italian
	Title            string // song name
	Total_length     string // seconds from first note to last note including breaks
	Version          string // difficulty name
	File_md5         string
	Mode             string // game mode,
	Tags             string // Beatmap tags separated by spaces.
	Favourite_count  string // Number of times the beatmap was favourited. (americans: notice the ou!)
	Rating           string
	Playcount        string // Number of times the beatmap was played
	Passcount        string // Number of times the beatmap was passed, completed (the user didn't fail or retry)
	Max_combo        string // The maximum combo a user can reach playing this beatmap.
}
